import React, { useContext } from 'react';
import Context from '../../context';

const TodoItem = (props) => {
  const { removeTodo, toggleTodo } = useContext(Context);

  return (
    <li className="todo__item">
      <label className={props.todo.completed ? 'done' : ''}>
        <input
          type="checkbox"
          className="todo__checkbox"
          checked={props.todo.completed}
          onChange={() => toggleTodo(props.todo.id)}
        />
        <span>{props.todo.title}</span>
      </label>
      <button
        type="button"
        className="todo__delete"
        onClick={() => removeTodo(props.todo.id)}
      ></button>
    </li>
  );
};

export default TodoItem;
